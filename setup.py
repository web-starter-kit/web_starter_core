# !/usr/bin/env python
import os
from distutils.core import setup, find_packages


with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='wagtail-web-starter-utility',
    packages=find_packages(),
    include_package_data=True,
    version='0.1.0',
    description='This is a mini app that provides common functionalities for Ayira wagtail web starter.',
    author='Ighor Jesse',
    license='BSD',
    author_email='jesse@ayira',
    url='https://gitlab.com/web-starter/core',
    keywords=['django', 'wagtail', 'ayira', 'quickstart'],
    classifiers=[
        'Framework :: Wagtail',
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Software Development',
    ],
)
