from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'web_starter_core'
